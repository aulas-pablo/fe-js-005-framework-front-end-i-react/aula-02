
# aula-02

O objetivo é iniciar um projeto React e construir os primeiros components utilizando o conceito de `props`.


Requisitos:
- Deverá iniciar um projeto react com o nome `store`

```bash
    npx create-react-app store
```

- Deverá criar um componente `header` seguindo o mesmo design-system da `aula-01`
- Deverá criar um componente button genérico que irá renderizar dinamicamente o `title` e a `cor` através de `props` seguindo o padrao:

- success = green
- warning = yellow
- danger = red